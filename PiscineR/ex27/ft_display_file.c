/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/08 17:01:51 by yazzedin          #+#    #+#             */
/*   Updated: 2016/12/14 15:27:09 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

void	display_file(int fd)
{
	char buf[1];

	while (read(fd, &buf, 1) > 0)
		write(1, &buf, 1);
}

int		main(int ac, char **av)
{
	int fd;

	if (ac > 2)
		write(2, "Too many arguments.\n", 20);
	else if (ac < 2)
		write(2, "File name missing.\n", 19);
	else
	{
		fd = open(av[1], O_RDONLY);
		if (fd == -1)
			return (-1);
		display_file(fd);
		close(fd);
	}
	return (0);
}
