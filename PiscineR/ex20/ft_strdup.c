/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 16:52:17 by yazzedin          #+#    #+#             */
/*   Updated: 2016/11/23 17:34:32 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *src)
{
	int j;

	j = 0;
	while (src[j])
		j++;
	return (j + 1);
}

char	*ft_strdup(char *src)
{
	int i;
	char*dup;

	i = 0;
	if (!src)
		return (NULL);
	dup = (char *)malloc(sizeof(char) * ft_strlen(src));
	while (src[i])
	{
		dup[i] = src[i];
		i++;
	}
	dup[i] = '\0';
	return (dup);
}
