/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/15 17:09:59 by yazzedin          #+#    #+#             */
/*   Updated: 2016/11/15 18:10:41 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_putchar(char c);

void	ft_print_params(char *argv)
{
	int j;

	j = 0;
	while (argv[j] != '\0')
	{
		ft_putchar(argv[j]);
		j++;
	}
}

int		main(int argc, char **argv)
{
	int i;
	int j;

	i = 1;
	j = 0;
	while (i < argc && argv[i][j] != '\0')
	{
		ft_print_params(&argv[i][j]);
		ft_putchar('\n');
		i++;
	}
	return (0);
}
