/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/15 18:14:56 by yazzedin          #+#    #+#             */
/*   Updated: 2016/11/23 16:50:17 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_putchar(char c);

int		ft_charcmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i] || s2[i])
	{
		if (s1[i] > s2[i])
			return (1);
		i++;
	}
	return (0);
}

void	ft_print_params(char *str)
{
	int i;

	i = 0;
	while (str[i])
		ft_putchar(str[i++]);
	ft_putchar('\n');
}

int		main(int argc, char **argv)
{
	int	x;
	char*swap;

	x = 1;
	while (x < argc - 1)
	{
		if (ft_charcmp(argv[x], argv[x + 1]) > 0)
		{
			swap = argv[x];
			argv[x] = argv[x + 1];
			argv[x + 1] = swap;
			x = 1;
		}
		else
			x++;
	}
	x = 0;
	while (++x < argc)
		ft_print_params(argv[x]);
	return (0);
}
