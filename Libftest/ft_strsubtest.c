#include <stdio.h>
#include <stdlib.h>

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	size_t i;

	i = 0;
	while (i < n)
	{
		((char *)dest)[i] = ((char*)src)[i];
		i++;
	}
	return (dest);
}

void	*ft_memset(void *str, int c, size_t n)
{
	unsigned char *ptr;

	ptr = str;
	while (n-- > 0)
		ptr[n] = c;
	return (str);
}

void	*ft_memalloc(size_t size)
{
	char *mem;

	mem = (char *)malloc(sizeof(char) * size);
	if (mem == 0)
		return (NULL);
	else
		return (ft_memset(mem, 0, size));
}

char    *ft_strnew(size_t size)
{
        char *str;

        str = (char *)ft_memalloc(size + 1);
        if (str)
                return (str);
        else
                return (NULL);
}

char 	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char *tr;
	int nel;

	nel = len;
	if (len > 0)
		tr = ft_strnew(nel);
	if (nel == 0)
		tr = ft_strnew(0);
	if (!s || !tr)
		return (NULL);
	s = s + start;
	if (tr)
		ft_memcpy(tr, s, nel);
	tr[nel] = '\0';
	return (tr);
}

char 	*ft_strcpy(char *dest, char *s)
{
	while (*s)
		*dest++ = *s++;
	*dest++ = '\0';
	return (dest);
}

int	main()
{
	char *s;
	char *tr;

	s = (char *)malloc(sizeof(char) * 1);
	ft_strcpy(s, "ssasa");
	printf("string is %s\n", s);

	printf("tronc is %s\n", ft_strsub(s, 1, (size_t)-10));
	return (0);
}
