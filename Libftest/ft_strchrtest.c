#include <stdio.h>
#include <string.h>

char	*ft_strrchr(const char *str, int c)
{
	char *ptr;

	while (*str)
	{
		if (*str == c)
			ptr = (char *)str;
		str++;
	}
	if (*ptr == c)
		return (ptr);
	else
		return (0);
}

char	*ft_strchr(const char *str, int c)
{
	while (*str && *str!= c)
		str++;
	if (*str == c)
		return ((char *)str);
	else
		return (0);
}

int main ()
{
   const char str[] = "Hello y Holly Hell";
   const char c = 'y';
   char *ret;

   ret = ft_strchr(str, c);
   printf("String after |%c| is - |%s|\n", c, ret);
   ret = ft_strrchr(str, c);
   printf("String after last |%c| is - |%s|\n", c, ret);
   
   return (0);
}
