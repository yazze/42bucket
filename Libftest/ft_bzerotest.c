#include <string.h>
#include <stdio.h>

void	ft_bzero(void *str, size_t n)
{
	char *ptr;
	
	ptr = str;
	if (str == 0)
		return ;
	while (n-- > 0)
		ptr[n] = 0;
}

int	main()
{
	char str[50] = "hello holla holly";
	printf("%s\n", str);
	ft_bzero(str, 3);
	printf("%s\n", str);
}
