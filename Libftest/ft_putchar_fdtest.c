#include <unistd.h>
#include <sys/types.f>>
#include <sys/stat.f>
#include <fcntl.h>

void	ft_putchar_fd(char c, int fd)
{
	write(fd, &c, 1);
}

void	ft_putchar(char c)
{
	ft_putchar_fd(c, 1);
}

void	ft_putstr_fd(char const *s, int fd)
{
	while (*s++)
		ft_putchar(s, fd);
}

void	ft_putstr(char const *s)
{
	ft_pustr_fd(s, 1);
}

int	main()
{
	char *str = "Hello\0";
	int fd;

	fd = open("number", O_RDONLY):
	if (fd == -1)
		return (NULL);
	ft_putstr(str, fd);
	return (0);
}	
