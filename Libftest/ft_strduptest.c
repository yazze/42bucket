/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/06 19:55:11 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/06 20:50:21 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int     ft_strlen(const char *s)
{
	int i = 0;
	while (s[i])
		i++;
	return (i);
}

char    *ft_strcpy(char *dest, const char *src)
{
	size_t i;
	
	i = 0;
	while (src[i])
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

char	*ft_strdup(const char *s)
{
	size_t i;
	char *s2;

	i = 0;
	s2 = (char *)malloc(ft_strlen(s) + 1);
	if (s2 != NULL)
		ft_strcpy(s2, s);
	else
		return (0);
	free(s2);
	return (s2);
}

int		main()
{
	char src[40] = "Hello Holla Hill";
	char dest[50];

	ft_strcpy(dest, src);
	printf("Result is: %s\n", dest);
	printf("Copied is: %s\n", ft_strdup(dest));
}
