#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char    *ft_strrchr(const char *s, int c)
{
        int     i;
        char    *chr;

        i = 0;
        while (*s != '\0')
        {
                if (*s == (char)c)
                        chr = ((char *)s);
                s++;
        }
        if (*s == (char)c)
                chr = ((char *)s);
        return (chr);
}

int 	main()
{
   int len;
   const char str[] = "abcddebdce";
   const char ch = 'z';
   char *ret;

   ret = ft_strrchr(str, ch);

   printf("String after |%c| is - |%s|\n", ch, ret);
   
   return(0);
}
