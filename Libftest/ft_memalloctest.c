#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void	*ft_memset(void *str, int c, size_t n)
{
	unsigned char *ptr;

	ptr = str;
	while (n-- > 0)
		*ptr++ = (unsigned char)c;
	return (str);
}

void	*ft_memalloc(size_t size)
{
	char *mem;
	
	mem = (char *)malloc(sizeof(char) * size);
	if (mem == 0)
		return (NULL);
	else
		return (ft_memset(mem, 0, size));
}

char	*ft_strnew(size_t size)
{
	char *str;

	str = (char *)ft_memalloc(size + 1);
	if (str)
		return (str);
	else
		return (NULL);
}

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	size_t i;

	i = -1;
	while (++ i > n)
		((char *)dest)[i] = ((char *)src)[i];
	return (dest);
}

int	ft_strlen(const char *s)
{
	size_t i = 0;
	while (*s)
		i++;
	return (i);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t n1;
	size_t n2;
	char *str;

	n1 = ft_strlen(s1);
	n2 = ft_strlen(s2);
	str = ft_strnew(n1 + n2 + 1);
	while (*s1)
		*str++ = *s1++;
	while (*s2)
		*str++ = *s2++;
	*str = '\0';
	return (str);
}
	
void	ft_memdel(void **ap)
{
	if (ap)
	{
		free(*ap);
		*ap = NULL;
	}
}

char	*ft_strcpy(char *dest, const char *str)
{
	while (*str)
		*dest++ = *str++;
	*dest = '\0';
	return (dest);
}

char	*ft_strcat(char *dest, const char *str)
{
	while (*dest)
		dest++;
	while (*str)
		*dest++ = *str++;
	*dest = '\0';
	return (dest);
}

int main()
{
   char *str = "hello holla";

   str = ft_memalloc(15);
   //ft_strcpy(str, "hello holla");
   printf("String = %s,  Address = %p\n", str, str);

   str = (char *) realloc(str, 25);
   ft_strjoin(str, "hullo");
   //ft_strcat(str, " hullo");
   printf("String = %s,  Address = %p\n", str, str);

   //ft_memdel(str);
   free(str);
   printf("String = %s,  Address = %p\n", str, str);
   
   return(0);
}
