#include <string.h>
#include <stdio.h>

int     ft_memcmp(const void *s1, const void *s2, size_t n)
{
        unsigned char *ptr1;
        unsigned char *ptr2;

        ptr1 = (unsigned char*)s1;
        ptr2 = (unsigned char*)s2;
        while (n > 0 && *ptr1 == *ptr2)
        {
                ptr1++;
                ptr2++;
                n--;
        }
        if (n == 0)
                return (0);
        Else
                return (*ptr1 - *ptr2);
}

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	size_t i;
	char *ptr1;
	const char *ptr2;

	ptr1 = dest;
	ptr2 = src;
	i = -1;
	while (++i < n)
		ptr1[i] = ptr2[i];
	return (dest);
}

int 	main()
{
	char str1[15];
	char str2[15];
	int dest;

	memcpy(str1, "hello", 6);
	memcpy(str2, "hello", 6);

	dest = memcmp(str1, str2, 5);
	if (dest > 0)
		printf("str2 is less than str1");
	else if (dest < 0)
		printf("str1 is less than str2");
	else
		printf("str1 is equal str2");
	return (0);
}
