#include <stdio.h>
#include <stdlib.h>

void	*ft_memset(void *str, int c, size_t n)
{
	unsigned char *ptr;

	ptr = str;
	while (n-- > 0)
		ptr[n] = c;
	return (str);
}

void	*ft_memalloc(size_t size)
{
	char *mem;

	mem = (char *)malloc(sizeof(char) * size);
	if (mem == 0)
		return (NULL);
	else
		return (ft_memset(mem, 0, size));
}

char    *ft_strnew(size_t size)
{
        char *str;

        str = (char *)ft_memalloc(size + 1);
        if (str)
                return (str);
        else
                return (NULL);
}

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	size_t i;

	i = 0;
	while (i < n)
	{
		((char *)dest)[i] = ((char*)src)[i];
		i++;
	}
	return (dest);
}

char 	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char *tr;
	int nel;

	nel = len;
	if (len > 0)
		tr = ft_strnew(nel);
	if (nel == 0)
		tr = ft_strnew(0);
	if (!s || !tr)
		return (NULL);
	s = s + start;
	if (tr)
		ft_memcpy(tr, s, nel);
	tr[nel] = '\0';
	return (tr);
}

int	ft_strlen(const char *src)
{
	unsigned int i;

	i = 0;
	while (src[i])
		i++;
	return (i);
}

char	*ft_strtrim(char const *s)
{
	char *trim;
	char const *ptr;
	char const *end; 
	int len;
	int i;

	len = 0;
	i = 0;
	while (*s == ' '|| *s == '\n' || *s == '\t')
		s++;
	if (*s == '\0')
		return (ft_strnew(0));
	ptr = s;
	end = s;
	while (*ptr != '\0')
	{
		if (*ptr != ' ' && *ptr == '\n' && *ptr != '\t')
		{
			i++;
		}
		ptr++;
	}
	len = ptr - end;
	printf("%d\n", len);
	trim = ft_strsub(s, 0, i);
	return (trim);
}
	
char	*ft_strcpy(char *dest, char *src)
{
	while (*src)
		*dest++ = *src++;
	return (dest);
}

int	main()
{
	char *s;

	s = (char *)malloc(sizeof(char) * 50);
	ft_strcpy(s, "\t\n  \tAAA \t BBB\t\n  \t");
	printf("string is %s\n", s);
	
	printf("trim is %s", ft_strtrim(s));
	return (0);
}
