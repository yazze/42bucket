#include <string.h>
#include <stdio.h>

void	*ft_memset(void *str, int c, size_t n)
{
	unsigned char *ptr;

	ptr = str;
	while (n-- > 0)
		ptr[n] = c;
	return (str);
}

int	main()
{
	char str[50] = "helllo holla holly";
	printf("%s\n", str);
	ft_memset(str, 45, 3);
	printf("%s\n", str);
} 
