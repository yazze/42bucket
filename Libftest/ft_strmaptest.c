#include <string.h>
#include <stdio.h>
#include <stdlib.h>

char	fzero(unsigned int i, char c)
{
	c = 'x';
	return (c);
}

void	*ft_memset(void *str, int c, size_t n)
{
	unsigned char *ptr;

	ptr = (unsigned char *)str;
	while (n-- > 0)
		*ptr++ = (unsigned char)c;
	return (str);
}

void	*ft_memalloc(size_t size)
{
	char *new;

	new = (char *)malloc(sizeof(char) * size);
	if (new == 0)
		return (NULL);
	else
		return (ft_memset(new, 0, size));
}

int	ft_strlen(const char *s)
{
	size_t i = 0;

	while (s[i])
		i++;
	return (i);
}

char    *ft_strcpy(char *dest, const char *s)
{
        while (*s)
                *dest++ = *s++;
        *dest = '\0';
        return (dest);
}

char	*ft_strmap(char const *s, char (*f)(char))
{
	char *new;
	char *ptr;

	new = ft_memalloc(ft_strlen(s));
	ptr = new;
	if (new == 0)
		return (NULL);
	while (*s)
		*new++ = f(*s++);
	return (ptr);
}

char	*ft_strmapi(char const *s, char(*f)(unsigned int, char))
{
	char *new;
	char *ptr;
	unsigned int i;

	i = 0;
	new = ft_memalloc(ft_strlen(s));
	ptr = new;
	if (new == 0)
		return (NULL);
	while (*s)
		*new++ = f(i++, *s++);
	return (ptr);
}

int	main()
{
	char *str;

	str = (char *)malloc(sizeof(char) * 6);
	ft_strcpy(str, "hello");
	printf("%s\n", str);

	printf("%s\n", ft_strmapi(str, fzero));
	return (0);
}
