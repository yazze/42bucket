#include <stdio.h>
#include <string.h>

int	ft_strcmp(const char *s1, const char *s2)
{
	if (s1 == 0 || s2 == 0)
		return (0);
	while ((*s1 || *s2) && (*s1 == *s2))
        {
                s1++;
                s2++;
        }
	return ((unsigned char)*s1 - (unsigned char)*s2);
}

int   	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	if (s1 == 0 || s2 == 0 || n == 0)
		return (0);
	while ((*s1 || *s2) && (*s1 == *s2) && --n > 0)
	{
		s1++;
		s2++;
	}
        return ((unsigned char)*s1 - ((unsigned char)*s2));
}

char	*ft_strcpy(char *dest, const char *src)
{
	while (*src)
		*dest++ = *src++;
	*dest++ = '\0';
	return (dest);
}

int main ()
{
   char str1[15];
   char str2[15];
   int ret;
   int ex;

   ft_strcpy(str1, "\200");
   ft_strcpy(str2, "\0");

   printf("%d\n", ret = ft_strncmp(str1, str2, 1));
   printf("%d\n", ex = strncmp(str1, str2, 1));

   if(ret < 0)
      printf("str1 is less than str2\n");
   else if (ret > 0) 
      printf("str2 is less than str1\n");
   else 
      printf("str1 is equal to str2\n");
   
   if(ex < 0)
      printf("str1 is less than str2");
   else if (ex > 0)
      printf("str2 is less than str1");
   else
      printf("str1 is equal to str2");

   return(0);
}
