#include <stdio.h>
#include <stdlib.h>

void    *ft_memset(void *str, int c, size_t n)
{
        unsigned char *ptr;

        ptr = str;
        while (n-- > 0)
                *ptr++ = (unsigned char)c;
        return (str);
}

void    *ft_memalloc(size_t size)
{
        char *mem;

        mem = (char *)malloc(sizeof(char) * size);
        if (mem == 0)
                return (NULL);
        else
                return (ft_memset(mem, 0, size));
}

char    *ft_strnew(size_t size)
{
        return ((char *)ft_memalloc(size + 1));
}

int     ft_strlen(const char *s)
{
	size_t i = 0;
        while (*s++)
                i++;
        return (i);
}

char    *ft_strjoin(char const *s1, char const *s2)
{
        size_t n1;
        size_t n2;
        char *str;
	char *ptr;

        n1 = ft_strlen(s1);
        n2 = ft_strlen(s2);
        str = ft_strnew(n1 + n2 + 1);
	ptr = str;
        while (*s1)
                *ptr++ = *s1++;
        while (*s2)
                *ptr++ = *s2++;
        *ptr = '\0';
        return (str);
}

int	main()
{
	char *str = "hello";
	char *dest = "holla";
	char *new;

   	printf("String = %s,  Address = %p\n", str, str);

   	new = ft_strjoin(str, dest);
   	printf("String = %s,  Address = %p\n", new, new);
   	return(0);
}
