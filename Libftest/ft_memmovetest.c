#include <string.h>
#include <stdio.h>
#include <stdlib.h>

char	*ft_strncpy(char *dest, const char *src, size_t n)
{
	char *ptr;
	
	ptr = dest;
	while (*src && n-- > 0)
		*dest++ = *src++;
	*dest = '\0';
	return (ptr);
}	 

void    *ft_memmove(void *dest, const void *src, size_t n)
{
	char *new;
	
	new = (char *)malloc(sizeof(char) * n);
	if (!new)
		return (NULL);
	new = ft_strncpy(new, src, n);
	printf("%s\n", new);
	dest = ft_strncpy(dest, new, n);
	printf("%s\n", dest);
	free(new);
	return (dest);
}

int     ft_strlen(const char *src)
{
        int i;

        i = 0;
        while (src[i])
                i++;
        return (i);
}

int     main()
{
        const char src[] = "newstring";
        char dest[] = "string";

	printf("here is dest = %s, src = %s\n", dest, src);
	ft_memmove(dest, src, ft_strlen(src)+1);
	printf("here is dest = %s, src = %s\n", dest, src);
}
