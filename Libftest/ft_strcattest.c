#include <stdio.h>
#include <string.h>

char	*ft_strncat(char *dest, const char *src, size_t n)
{
	size_t i;

	i = 0;
	while (*dest)
		dest++;
	while (i++ < n && *src)
		*dest++ = *src++;	
	*dest = '\0';
	return (dest);
}

char	*ft_strcat(char *dest, const char *src)
{
	while (*dest)
		dest++;
	while (*src)
		*dest++ = *src++;
	*dest = '\0';
	return (dest);
}
	
char	*ft_strcpy(char *dest, const char *src)
{
	size_t i;

	i = 0;
	while (src[i])
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

int	main()
{
	char src[50], dest[50];

	ft_strcpy(src, "this is source");
	printf("%s\n", src);
	ft_strcpy(dest, "this is destination");
	printf("%s\n", dest);

	ft_strcat(dest, src);
	printf("Final result is : |%s|\n", dest);
	ft_strncat(dest, src, 5);
	printf("Final result is : |%s|\n", dest);
	return (0);
}
