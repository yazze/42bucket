#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void	ft_printer(char **tab)
{
	printf("%s\n", tab[0]);
	printf("%s\n", tab[1]);
	printf("%s\n", tab[2]);
	printf("%s\n", tab[3]);
	printf("%s\n", tab[4]);
	printf("%s\n", tab[5]);
}

int	ft_strlen(const char *src)
{
	int i;

	i = 0;
	while (src[i])
		i++;
	return (i);
}

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	size_t	i;

	i = -1;
	while (++i < n)
		((char *)dest)[i] = ((char *)src)[i];
	return (dest);
}

char 	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char *str;

	str = (char *)malloc(sizeof(char) * (len + 1));
	if (s == 0)
		return (NULL);
	s = s + start;
	ft_memcpy(str, s, len);
	str[len] = '\0';
	return (str);
}

char		**ft_strsplit(const char *s, char c)
{
	size_t	i;
	size_t	j;
	int	start;
	int	end;
	char	**tab;

	tab = 0;
	i = 0;
	j = 0;
	if (s && (tab = (char **)malloc(sizeof(*tab) * (ft_strlen(s)))))
	{
		while (s[i])
		{
			while (s[i] && s[i] == c)
				i++;
			start = i;
			while (s[i] && s[i] != c)
				i++;
			end = i;
			if (end > start)
				tab[j++] = ft_strsub(s, start, (end - start));
		}
	}
	tab[j] = NULL;
	return (tab);
}

char		*ft_strcpy(char *dest, const char *src)
{
	size_t	i;

	i = 0;
	while (src[i])
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

int	main()
{
	char *s;

	s = (char *)malloc(sizeof(char) * 25);
        ft_strcpy(s, "hello commont ollez vous");
	ft_printer(ft_strsplit(s, 'o'));
	return (0);
}
