#include <stdio.h>
#include <string.h>

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	const char *ptr1;
	const char *ptr2;

	while (*s1 && n-- > 0)
	{
		ptr1 = s1;
		ptr2 = s2;
		while (*ptr1 == *ptr2)
		{
			ptr1++;
			ptr2++;
		}
		if (*ptr2 == '\0')
			return((char *)s1);
		s1++;
	}
	return (0);
}

char    *ft_strstr(const char *s1, const char *s2)
{
        char    *string;

        if (s2 == NULL || ft_strlen(s2) == 0)
                return ((char *)s1);
        while (*s1)
        {
                if (ft_strncmp((char *)s1, s2, ft_strlen(s2)) == 0)
                        return ((string = (char *)s1));
                s1++;
        }
        return (NULL);
}

int main()
{
   const char hay[20] = "ozarabozaraboze";
   const char nee[10] = "ozaraboze";
   char *ret;
   char *true;

   ret = ft_strstr(hay, nee);
   true = strstr(hay, nee);

   printf("The substring is: %s\n", ret);
   printf("The substring is: %s\n", true);
   return (0);
}
