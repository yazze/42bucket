#include <stdio.h>
#include <stdlib.h>

void	ft_strclr(char *s)
{
	while (*s)
		*s++ = '\0';
}

void	ft_striter(char *s, void (*f)(char *))
{
	while (*s)
		f(s++);
}

void	ft_striteri(char *s, void (*f)(unsigned int, char *))
{
	unsigned int n;
	
	n = 0;
	while (*s)
		f(n++, s++);
}

char	*ft_strcpy(char *dest, const char *src)
{
	while (*src)
		*dest++ = *src++;
	*dest = '\0';
	return (dest);
}

int	main()
{
	char *str = (char *)malloc(sizeof(char) * 6);
	ft_strcpy(str, "Hello");
	printf("string is %s, address is %p\n", str, str);

	ft_striteri(str, ft_strclr);
	printf("string is %s, address is %p\n", str, str);
	
	return (0);
}
