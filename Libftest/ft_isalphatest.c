#include <stdio.h>
#include <ctype.h>

int	ft_isalpha(int c)
{
	if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
		return (c);
	else
		return (0);
}

int	ft_isdigit(int c)
{
	if (c >= '0' && c <= '9')
		return (c);
	else
		return (0);
}

int	ft_isalnum(int c)
{
	if (ft_isalpha(c) || ft_isdigit(c))
		return (c);
	else
		return (0);
}

int	ft_isascii(int c)
{
	if (c >= 'null' && c <= 'del')
		return (c);
	else
		return (0);
}

int	ft_isprint(int c)
{
	if (c >= ' ' && c <= 'del')
		return (c);
	else
		return (0);
}

int main()
{
   int var1 = 'd';
   int var2 = '2';
   int var3 = 'A';
   int var4 = '8';
   int var5 = '2';
   int var6 = '\t';
    
   if( ft_isalpha(var1) )
      printf("var1 = |%c| is an alphabet\n", var1 );
   else
      printf("var1 = |%c| is not an alphabet\n", var1 );
   
   if( ft_isalpha(var2) )
      printf("var2 = |%c| is an alphabet\n", var2 );
   else
      printf("var2 = |%c| is not an alphabet\n", var2 );
   
   if( ft_isdigit(var3) )
      printf("var3 = |%c| is a digit\n", var3 );
   else
      printf("var3 = |%c| is not a digit\n", var3 );
   
   if( ft_isdigit(var4) )
      printf("var4 = |%c| is a digit\n", var4 );
   else
      printf("var4 = |%c| is not a digit\n", var4 );

   if( ft_isalnum(var5) )
      printf("var5 = |%c| is an alphanun\n", var5 );
   else
      printf("var5 = |%c| is not an alphanum\n", var5 );

   if( ft_isalnum(var6) )
      printf("var6 = |%c| is an alphanum\n", var6 );
   else
      printf("var6 = |%c| is not an alphanum\n", var6 );

   return(0);
}
