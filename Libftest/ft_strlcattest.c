#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int	ft_strlen(const char *s)
{
	size_t i = 0;

	while (s[i])
		i++;
	return (i);
}

size_t	ft_strlcat(char *dest, const char *src, size_t size)
{
	size_t	i;
	size_t	n1;
	size_t	n2;

	i = 0;
	n1  = 0;
	n2 = ft_strlen(dest);
	while (dest[n1] && n1 < size)
		n1++;
	if (!(n1 < size))
		return (size + ft_strlen(src));
	while (src[i] && i < (size - n2 - 1))
	{
		dest[n1 + i] = ((char *)src)[i];
		i++;
	}
	dest[n1 + i] = '\0';
	return (n1 + ft_strlen(src));
}

void	*ft_strcpy(char *dest, char *s)
{
	while (*s)
		*dest++ = *s++;
	*dest = '\0';
	return (dest);
}

int	main()
{
	char *str;
	char *dest;
	char *strp;
	char *destp;
	size_t i = 10;

	str = (char *)malloc(sizeof(char) * 5);
	dest = (char *)malloc(sizeof(char) * 5);
	strp = str;
	destp = dest;
	ft_strcpy(str, "Hello");
	ft_strcpy(dest, "Holla");
	printf("Str is %s, dest is %s\n", str, dest);

	printf("%zu\n", ft_strlcat(destp, strp, i));
	return (0);
}
