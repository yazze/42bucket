#include <stdlib.h>
#include <stdio.h>

void	fzero(unsigned int i, char *s)
{
	*s = 'x';
}

void	ft_striter(char *s, void (*f)(char *))
{
	while (*s)
		f(s++);
}

void	ft_striteri(char *s, void (*f)(unsigned int, char *))
{
	unsigned int i;

	i = 0;
	while (s[i])
	{
		f(i, &s[i]);
		i++;
	}
}
		
char	ft_strcpy(char *dest, char *s)
{
	while (*s)
		*dest++ = *s++;
	*dest ='\0';
	return (*dest);
}
		
int	main()
{
	char *str;
	str = (char *)malloc(sizeof(char) * 6);
	ft_strcpy(str, "hello");
	printf("%s\n", str);

	ft_striteri(str, fzero);
	printf("%s\n", str);
	return (0);
}
