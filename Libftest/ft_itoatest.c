#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char	*ft_strcpy(char *dest, const char *s)
{
	while (*s)
		*dest++ = *s++;
	*dest = '\0';
	return (dest);
}

int	ft_strlen(const char *s)
{
	int i;

	i = 0;
	while (s[i])
		i++;
	return (i);
}

char	*ft_strdup(const char *s)
{
	size_t	i;
	char	*s2;

	i = 0;
	s2 = (char *)malloc(ft_strlen(s) + 1);
	if (s2 != NULL)
		ft_strcpy(s2, s);
	else
		return (0);
	free(s2);
	return (s2);
}

void	*ft_memset(void *str, int c, size_t n)
{
	unsigned char *ptr;

	ptr = str;
	while (n-- > 0)
		*ptr++ = (unsigned char)c;
	return (str);
}

void	*ft_memalloc(size_t size)
{
	char *mem;

	mem = (char *)malloc(sizeof(char) * size);
	if (mem == 0)
		return (NULL);
	else
		return (ft_memset(mem, 0, size));
}

char	*ft_itoa(int n)
{
	char	*nb;
	int 	i;

	i = 1;
	if (n < 0)
		i = -1;
	if ((nb = (char *)ft_memalloc(10)))
	{
		if (n == 0)
                	return (ft_strdup(nb = "0"));
		while (n > 0)
		{
			*--nb = '0' + (i * (n % 10));
			n /= 10;
		}
	}
	else
		return (0);
	if (i == -1)
		*--nb = '-';
	return (ft_strdup(nb));
}

int	main()
{
	int n = 0;

	printf("Entier is %s\n", ft_itoa(n));
	return (0);
}
