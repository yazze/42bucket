#include <string.h>
#include <stdio.h>
#include <unistd.h>
	
void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	size_t i;

	i = -1;
	while (++i < n)
		((char *)dest)[i] = ((char *)src)[i];
	return (dest);
}


int	ft_strlen(char *src)
{
	int i;

	i = 0;
	while (src[i])
		i++;
	return (i);
}

int	main()
{
	char src[50] = "hello holla helli";
	char dest[50];

	printf("here is = %s\n", dest);
	ft_memcpy(dest, src, ft_strlen(src)+1);
	printf("here is = %s\n", dest);
}
