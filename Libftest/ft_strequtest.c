#include <stdio.h>
#include <stdlib.h>

char    ft_strncmp(const char *s1, const char *s2)
{
        while (*s1++ && *s2++)
                if (*s1 != *s2)
                        return (*s2 - *s1);
        return (0);
}

int	ft_strequ(char const *s1, char const *s2)
{
	if (ft_strcmp(s1, s2) == 0)
                return (1);
        else
                return (0);
}

char	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	while (*s1++ && *s2++ && --n > 0)
		if (*s1 != *s2)
			return (*s2 - *s1);
	return (0);
}

int     ft_strnequ(char const *s1, char const *s2, size_t n)
{
        if (ft_strncmp(s1, s2, n) == 0)
		return (1);
	else
		return (0);
}

void	*ft_strcpy(char *dest, char *src)
{
	while (*src)
		*dest++ = *src++;
	*dest++ = '\0';
	return (dest);
}

int	main()
{
	char *s1;
	char *s2;

	s1 = (char *)malloc(sizeof(char) * 6);
	s2 = (char *)malloc(sizeof(char) * 4);
	ft_strcpy(s1, "Hello");
	ft_strcpy(s2, "Hey");

	if (ft_strequ(s1, s2) == 1)
		printf("%s\n", "Strings Are Equal");
	else
		printf("%s\n", "Strings are different");
}	
