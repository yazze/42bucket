#include <stdio.h>
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	ft_toupper(int c)
{
	if (c >= 'a' && c <= 'z')
		return (c - ' ');
	else
		return (c);
}

int	ft_tolower(int c)
{
	if (c >= 'A' && c <= 'Z')
                return (c + ' ');
        else
                return (c);
}

int main()
{
   int i = 0;
   char str[] = "hello holla";
   
   printf("%s\n", str);
   while(str[i])
      ft_putchar(ft_toupper(str[i++]));
   ft_putchar('\n');
   i = 0;
   while(str[i])
   	ft_putchar(ft_tolower(str[i++]));
   
   return(0);
}
