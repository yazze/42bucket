#include <stdio.h>
#include <string.h>

void    *ft_memchr(const void *s, int c, size_t n)
{
        char *str;

        if (s == NULL)
                return (NULL);
        str = (char*)s;
        while (n > 0 && *str != (char)c)
        {
                n--;
                str++;
        }
        if (n == 0)
                return (NULL);
        else
                return (str);
}

int	ft_strlen(char *src)
{
	int i;
	i = 0;
	while (src[i])
		i++;
	return(i);
}

int	main()
{
	const char src[] = "newstring";
	const char sch = 's';
	char *dest;

	dest = memchr(src, sch, strlen(src));
	printf ("String after |%c| is - |%s|\n", sch, dest);
	return (0);
}
