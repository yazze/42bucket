#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int	ft_atoi(const char *str)
{
	int num;
	int j;

	j = 1;
	num = 0;	
	while (*str == '\t' || *str == '\n' || *str == '\r'
		|| *str == '\v' || *str == '\f' || *str == ' ')
		str++;	
	if (*str == '-')
	{
		j = -1;
		str++;
	}
	else if (*str == '+')
	{
		j = 1;
		str++;
	}
	if (!((*str >= '0' && *str <= '9')))
                        return (0);
	while (*str >= '0' && *str <= '9')
			num = num * 10 + (*str++ - '0');
	return (num * j);
}

char	*ft_strcpy(char *dest, const char *src)
{
	while (*src)
		*dest++ = *src++;
	*dest++ = '\0';
	return (dest);
}

int main()
{
   int val;
   char str[20];
   int ex;
   
   ft_strcpy(str, "-+1");
   val = ft_atoi(str);
   ex = atoi(str);
   printf("String = %s, Int = %d, Atoi = %d\n", str, val, ex);

   ft_strcpy(str, "hello holla");
   val = atoi(str);
   printf("String value = %s, Int value = %d\n", str, val);

   return(0);
}
