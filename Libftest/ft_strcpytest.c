#include <stdio.h>
#include <string.h>

char	*ft_strncpy(char *dest, const char *src, size_t n)
{
	size_t i;

	if (dest == 0 || src == 0)
		return (0);
	i = 0;
	while(src[i] && i < n)
	{
		dest[i] = src[i];
		i++;
	}
	return (dest);
}

char	*ft_strcpy(char *dest, const char *src)
{
	size_t i;

	i = 0;
	while (src[i])
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

void	*ft_memset(void *s, int c, size_t n)
{
	size_t i;

	i = 0;
	while (i < n)
	{
		((unsigned char *)s)[i] = c;
		i++;
	}
	return (s);
}

int main()
{
   char src[40];
   char dest[100];
  
   ft_memset(dest, '\0', sizeof(dest));
   ft_strcpy(src, "Hello holla");
   printf("Final copied string : %s\n", dest);
   ft_strncpy(dest, src, 8);
   printf("Final copied string : %s\n", dest);
   
   return(0);
}
