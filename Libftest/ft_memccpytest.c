#include <string.h>
#include <stdio.h>

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	size_t i;

	i = 0;
	while (i < n && ((char *)src)[i - 1] != c)
	{
		((char *)dest)[i] = ((char *)src)[i];
		i++;
	}
	if (((char *)src)[i] == c)	
		return (dest);
	else
		return (0);
}

int		ft_strlen(char *str)
{
	int i = 0;

	while (str[i])
		i++;
	return (i);
}

int	main()
{
	char src[50] = "hello holla helli";
	char dest[50] = "hello holla helli";

	printf("here is = %s\n", dest);
	ft_memccpy(dest, src, 'a', ft_strlen(src)+1);
	printf("here is = %s\n", dest);
}
