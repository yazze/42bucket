/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 13:46:20 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/14 22:13:46 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int			*ft_range(int min, int max)
{
	int		i;
	int		*tab;

	i = 0;
	tab = (int*)malloc(sizeof(int) * ((max - min) + 1));	
	if (min >= max)
		return (NULL);
	while (min < max)
	{
		tab[i] = min;
		printf("%d\n", tab[i]);
		i++;
		min++;
	}
	return (tab);
}

int			main(void)
{
	ft_range(-9, -5);
	return (0);
}
