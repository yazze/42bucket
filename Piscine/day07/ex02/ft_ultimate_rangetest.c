/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 16:29:58 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/14 18:48:20 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include <stdio.h>

int		ft_ultimate_range(int **range, int min, int max)
{
	int i;
	int *j;

	j = NULL;
	if (min >= max)
		return (*j);
	if (max > min)
	{
		i = 0;
		**range = *(int*)malloc(sizeof(int) * (max - min));
		while (i < max - min)
		{
		*range[i] = min + i;
		i++;
		}
		return (i);
	}
	else 
		return (0);
}

int		main()
{
	int *a;
	int *b;

	a = *b;
	b = 2;
	ft_ultimate_range(b, 5, 9);
	return (0);
}
