/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/13 17:33:37 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/14 11:56:48 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#define LEN		21

int		ft_putchar(char c)
{
	write(1, &c, 1);
	return (0);
}

void	ft_putstr(char *str)
{
	while(*str)
		ft_putchar(*str);
}

int		main()
{
	int i;

	str = (char*)malloc(sizeof(*str) * (LEN +1));
	i = 0;
	while (i < LEN)
	{
		str[i] = '0' + (i % 10);
		i++;
	}	
	str[i] = '\0';
	while (1);
	ft_putstr(str);
	ft_putstr("\n");
	return (0);
}
