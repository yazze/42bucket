/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_concat_params.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 18:53:24 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/14 21:19:42 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int			ft_strlen(int argc, char **argv)
{
	int i;
	int j;
	int l;

	l = 0;
	i = 0;
	j = 1;
	while (i < argc)
	{
		j = 0;
		while (argv[i][j] != '\0')
		{
			j++;
			l++;
		}
		i++;
		l++;
	}
	return (l);
}

char		*ft_concat_params(int argc, char **argv)
{
	int		i;
	int		a;
	int		c;
	char	*str;

	a = 1;
	i = 0;
	c = 0;
	str = (char*)malloc(sizeof(char) * ft_strlen(argc, argv));
	while (a < argc)
	{
		i = 0;
		while (argv[a][i] != '\0')
		{
			str[c] = argv[a][i];
			c++;
			i++;
		}
		str[c] = '\n';
		c++;
		a++;
	}
	return (str);
}
