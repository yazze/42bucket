/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/18 18:39:08 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/18 19:39:40 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_map(int *tab, int length, int (*f)(int))
{
	int i;
	int j;
	int *ptr;

	i = 0;
	j = 0;
	ptr = (int*)malloc(sizeof(int) * length);
	while (tab[i] < length)
	{
		ptr[j] = f(tab[i]);
		i++;
		j++;
	}
	return (ptr);
}
