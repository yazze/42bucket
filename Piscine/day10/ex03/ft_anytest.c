/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_any.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/18 22:04:47 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/19 14:08:10 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		f(char *tab)
{
	int i = 0;

	while (tab[i] != '\0')
	{
		if (tab[i] == 'e')
			return ('1');
		i++;
	}
	return (0);
}

int		ft_any(char **tab, int (*f)(char*))
{
	int i;

	i = 1;
	while (tab[i][0] != '0')
	{
		if (f(tab[i]) == '1')
			return (1);
		i++;
	}
	return (0);
}

int		main(int argc, char **argv)
{
		printf("%d\n", ft_any(argv, &f));
		return (0);
}
