/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/17 14:22:43 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/17 15:22:40 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FT_H__
#define __FT_H__

void	ft_putchar(char c);

void	ft_putstr(char *str);

void	ft_strcmp(char *s1, char *s2);

void	ft_strlen(char *str);

void	ft_swap(int *a, int *b);

#endif
