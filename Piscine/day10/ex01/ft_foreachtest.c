/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_foreach.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/18 17:19:20 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/18 18:07:31 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

typedef	void(*ptr)(int);

void	ft(int i)
{
	printf ("%d", i);
}

void	ft_foreach(int *tab, int lenght, void(*f)(int))
{
	int i;

	i = 0;
	while (tab[i] < lenght)
	{
		f(tab[i]);
		i++;
	}
}

int		main()
{
	ptr	f;
	int	*tab;

	tab = 5;
	f = &ft;
	ft_foreach(tab, 5, f);
	return (0);
}
