/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/09 15:32:19 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/09 20:26:58 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int		ft_recursive_factorial(int nb)
{
		if (nb > 0 && nb < 13)
			return (nb * ft_recursive_factorial(nb - 1));
		else if (nb == 0)
			return (1);
		else if (nb < 0 || nb > 13)
			return (0);
		return (0);
}

int		main()
{
	printf ("%d\n", ft_recursive_factorial(-10));
	return (0);
}
