/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 18:14:10 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/21 19:10:59 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_list.h"
#include <stdlib.h>

int		main(void)
{
	char *data;
	t_list *test;
	
	test = NULL;
//	test = ft_create_elem("feiofies");
	ft_list_push_back(&test, "eofiefi");
	ft_list_push_back(&test, "efi");
	ft_list_push_back(&test, "eofi");
	ft_list_push_back(&test, "eofiei");
	while (test != NULL)
	{
		printf("%s\n", test->data);
		test = test->next;
	}
	return (0);
}
