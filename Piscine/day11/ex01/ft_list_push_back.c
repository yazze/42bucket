/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 19:20:26 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/21 19:03:27 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

void	ft_list_push_back(t_list **begin_list, void *data)
{
	t_list	*new;

	new = *begin_list;
	if (new == NULL)
		*begin_list = ft_create_elem(data);
	else
	{
		while (new->next != NULL)
			new = new->next;
		new->next = ft_create_elem(data);
	}
}
