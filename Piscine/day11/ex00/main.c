/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 18:14:10 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/19 18:27:31 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_list.h"

int		main(int argc, char **argv)
{
	char *data;

	data = "hello";
	ft_create_elem(data);
	printf("%s\n", data);
	return (0);
}
