/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_last.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 20:30:12 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/20 21:05:54 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

t_list	*ft_list_last(t_list *begin_list)
{
	t_list	*copy;
	t_list	*temp;

	temp = begin_list;
	while (temp->next != NULL)
	{
		copy = temp;
		temp = temp->next;
	}
	return (copy);
}
