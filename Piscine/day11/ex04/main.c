/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 22:59:37 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/20 23:32:37 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdio.h>

int		main(void)
{
	t_list *test;
	char *data;
	char *ex;

	data = "ok";
	test = ft_create_elem("c");
	ft_list_push_back(&test, data);
	ft_list_push_back(&test, data);
	ft_list_push_back(&test, "last");
	*ex = ft_list_last(test);
	while (test!= NULL)
	{
		printf("%s\n", ex);
		test = test->next;
	}
	return (0);
}
