/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 18:14:10 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/20 23:04:25 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_list.h"
#include <stdlib.h>

int		main(void)
{
	char *data;
	t_list *test;

	test = ft_create_elem("feiofies");
	ft_list_push_front(&test, "eofiefi");
	while (test != NULL)
	{
		printf("%s\n", test->data);
		test = test->next;
	}
	return (0);
}
