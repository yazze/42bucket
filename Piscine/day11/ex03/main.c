/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 22:59:37 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/20 23:24:53 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdio.h>

int		main(void)
{
	t_list *test;
	char *data;

	data = "ok";
	test = ft_create_elem("c");
	ft_list_push_back(&test, data);
	ft_list_push_back(&test, data);
	ft_list_push_back(&test, data);
	while (test!= NULL)
	{
		printf("%d\n", ft_list_size(test));
		test = test->next;
	}
	return (0);
}
