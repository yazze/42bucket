/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_size.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 20:23:25 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/20 21:03:13 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

int		ft_list_size(t_list *begin_list)
{
	int		c;
	t_list	*temp;

	c = 0;
	temp = begin_list;
	while (temp->next != NULL)
	{
		c++;
		temp = temp->next;
	}
	return (c);
}
