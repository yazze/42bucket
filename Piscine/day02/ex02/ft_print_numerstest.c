/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_numers.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/07 16:54:59 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/07 18:22:48 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_putchar(char n)
{
	write(1, &n, 1);
	return (0);
}

void	ft_print_numers(void)
{
	char	n;
	int 	i;

	n = '0';
	i = 0;
	while (i <= 9)
	{
		ft_putchar(n);
		n++;
		i++;
	}
}

int		main()
{
	ft_print_numers();
	return (0);
}
