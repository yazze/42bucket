/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/12 17:43:20 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/12 20:42:51 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

int		ft_strncmp(char *s1, char *s2, unsigned int n)
{
	unsigned int i;

	i = 0;
	while (i < n && (s1[i] == s2[i]  && (s1[i] != '\0' || s2[i] != '\0')))
		i++;
	return (s1[i] - s2[i]);
}

int		main(void)
{
	char a[] = "helloholla";
	char b[] = "lohollb";
	int n = 2;

	printf("%d\n", ft_strncmp(a, b, n));
	printf("%d\n", strncmp(a, b, n));
	return(0);
}
