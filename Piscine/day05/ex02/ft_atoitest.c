/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/10 19:57:24 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/12 21:13:58 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int		ft_atoi(char *str)
{
	int i;
	int d;
	int nb;

	i = 0;
	d = 0;
	nb = 0;
	while (str[i] == ' ' || str[i] == '\t' || str[i] == '\n' || str[i] == '\r'
			|| str[i] == '\v' || str[i] == '\f')
		i++;
	if (str[i] == '-')
		d++;
	if (str[i] == '-' || str[i] == '+')
		i++;
	while (str[i] >= '0' && str[i] <= '9')
	{
		nb = nb * 10;
		nb = nb + (str[i] - '0');
		i++;
	}
	if (d == 1)
		return(-nb);
	else
		return(nb);
}

int		main()
{
	printf("ft_atoi : %d\n", ft_atoi("\n \t\f\v\r+05 0561"));
	printf("atoi    : %d\n", atoi("\n \t\f\v\r+05 0561"));
	printf("ft_atoi : %d\n", ft_atoi("+2"));
	printf("atoi    : %d\n", atoi("+2"));
	printf("ft_atoi : %d\n", ft_atoi("--2"));
	printf("atoi    : %d\n", atoi("--2"));
	printf("ft_atoi : %d\n", ft_atoi("-2147483650"));
	printf("atoi    : %d\n", atoi("-2147483650"));
	return (0);
}
