/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoitrain.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/11 16:51:42 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/11 17:15:06 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_atoi(char *str)
{
	int i;
	int j;
	int x;

	i = 0;
	j = 0;
	x = 0;
	if (str[i] != '-' && !(str[i] >= '0' && str[i] <= '9'))
		i++;
	if (str[i] == '-')
		j++;
	while (str[i] >= '0' && str[i] <= '9')
	{
		x = x * 10;
		x = x + (str[i] - '0');
		i++;
	}
	if (j == 1)
		return (-x);
	else
		return (x);
}

int		main()
{
	printf("%d\n", ft_atoi("-355"));
	return (0);
}
