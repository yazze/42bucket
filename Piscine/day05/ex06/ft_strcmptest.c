/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/12 15:03:03 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/12 18:36:05 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_strcmp(char *s1, char *s2)
{
		int i;

		i = 0;
		while (s1[i] == s2[i] && (s1[i] != '\0' || s2[i] != '\0'))
			i++;
		return (s1[i] - s2[i]);
}

int		main(void)
{
	char s1[] = "helloholla";
	char s2[] = "loh";
	printf ("%d\n", ft_strcmp(s1, s2));
	return (0);
}
