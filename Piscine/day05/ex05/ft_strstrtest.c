/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/12 16:57:04 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/12 17:35:43 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char		*ft_strstr(char *str, char *to_find)
{
	int i;
	int j;

	i = 0;
	while (str[i++])
	{
		j = 0;
		while (str[i + j] == to_find[j])
		{
			j++;
		}
		if (to_find[j] == '\0')
		{
			return (str + i);
		}
	}
	return (str);
}

int		main()
{
	char a[] = "hollahello";
	char b[] = "lahe";

	ft_strstr(a, b);
	printf("%s\n", ft_strstr(a, b));
	return (0);
}
