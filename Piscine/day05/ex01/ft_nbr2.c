#include <unistd.h>
#include <stdio.h>

int     ft_putchar(char c)
{
	write (1, &c, 1);
	return (0);
}

void    ft_putnbr(int nb)
{
	if (nb < 0)
	{
		ft_putchar('-');
		ft_putnbr(nb / -10);
		ft_putchar(0 - nb % 10 + '0');
	}
	if (nb >= 10)
	{
		ft_putnbr(nb / 10);
		ft_putchar(nb % 10 + '0');
	}
	else if (nb < 10 && nb >= 0)
		ft_putchar(nb % 10 + '0');
}

int     main()
{
	ft_putnbr(-2147483648);
	return (0);
}
