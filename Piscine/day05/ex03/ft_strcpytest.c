/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/11 17:17:20 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/12 22:21:12 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <string.h>

int		ft_putchar(char c)
{
	write(1, &c, 1);
	return (0);
}

int		ft_strlen(char *str)
{
	int l;

	l = 0;
	while (str[l] != '\0')
		l++;
	return (l);
}

char	*ft_strcpy(char *src, char *dest)
{
	int i;

	i = 0;
	if (ft_strlen(dest) < ft_strlen(src))
		return (0);
	while (src[i] != '\0')
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

int		main(void)
{
	char a[] = "hello";
	char b[] = "jell";

	printf("%s\n", a);
	printf("%s\n", b);
	ft_strcpy(a, b);
	strcpy(a, b);
	printf("%s\n", b);
	return (0);
}
