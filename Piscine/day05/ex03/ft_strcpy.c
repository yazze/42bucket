/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/11 17:17:20 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/11 19:45:12 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcpy(char *src, char *dest)
{
	int i;
	int x;
	int y;

	i = 0;
	x = 1;
	y = 1;
	while (dest[x] != '\0')
		x++;
	while (src[y] != '\0')
		y++;
	if (x < y)
		return (0);
	while (src[i] != '\0')
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}
