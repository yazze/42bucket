/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/10 19:57:24 by yazzedin          #+#    #+#             */
/*   Updated: 2016/07/16 20:44:08 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int		ft_atoi(char *str)
{
	int i;
	int nb;

	i = 0;
	nb = 0;
	while (str[i] > 33 && str[i] != '\0')
		i++;
	if (str[i] == '-')
	{	
		write(1, "-", 1);
		nb = -nb;
	}
	if (str[i] == '-' || str[i] == '+')
		i++;
	while (str[i] >= '0' && str[i] <= '9' && str[i] != '\0')
	{
		nb = nb * 10;
		nb = nb + (str[i++] - '0');
	}
	return (nb);
}

int		main()
{
	printf("%d\n", ft_atoi("455"));
	return (0);
}
