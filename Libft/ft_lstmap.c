/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 19:18:22 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/27 19:18:30 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*lst1;
	t_list	*lstend;
	t_list	*lstnew;

	lstnew = NULL;
	while (lst)
	{
		lstend = malloc(sizeof(size_t));
		if (!lstend)
			return (NULL);
		lstend = f(lst);
		if (!lstnew)
		{
			lstnew = lstend;
			lst1 = lstend;
		}
		else
		{
			lst1->next = lstend;
			lst1 = lst1->next;
		}
		lst = lst->next;
	}
	return (lstnew);
}
