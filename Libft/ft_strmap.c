/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 19:30:23 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/27 19:30:37 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char *new;
	char *ptr;

	if (!s || !f)
		return (NULL);
	if (!(new = ft_memalloc(ft_strlen(s) + 1)))
		return (NULL);
	ptr = new;
	while (*s)
		*new++ = f(*s++);
	*new++ = '\0';
	return (ptr);
}
