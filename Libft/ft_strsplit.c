/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 19:38:58 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/31 18:00:28 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		**ft_strsplit(const char *s, char c)
{
	size_t	i;
	size_t	j;
	size_t	len;
	char	**tab;

	i = 0;
	j = 0;
	len = 0;
	if (!s || !c || !(tab = (char **)malloc(sizeof(*tab) * (ft_strlen(s)))))
		return (NULL);
	while (s[i])
	{
		while (s[i] && s[i] == c)
			i++;
		len = i;
		while (s[i] && s[i] != c)
			i++;
		if (i > len)
			tab[j++] = ft_strsub(s, len, (i - len));
	}
	tab[j] = NULL;
	return (tab);
}
