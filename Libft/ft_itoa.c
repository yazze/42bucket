/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 19:19:15 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/27 19:24:10 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_itoa(int n)
{
	char	*nb;
	int		i;

	i = 1;
	if (n < 0)
		i = -1;
	if ((nb = (char *)ft_memalloc(10)))
	{
		if (n == 0)
			return (ft_strdup(nb = "0"));
		while (n != 0)
		{
			*--nb = '0' + (i * (n % 10));
			n /= 10;
		}
	}
	else
		return (NULL);
	if (i == -1)
		*--nb = '-';
	return (ft_strdup(nb));
}
