/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 19:31:23 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/27 19:42:47 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dest, const char *src, size_t size)
{
	size_t	i;
	size_t	n1;
	size_t	n2;

	i = 0;
	n1 = 0;
	n2 = ft_strlen(dest);
	while (dest[n1] && n1 < size)
		n1++;
	if (!(n1 < size))
		return (size + ft_strlen(src));
	while (src[i] && i < (size - n2 - 1))
	{
		dest[n1 + i] = ((char *)src)[i];
		i++;
	}
	dest[n1 + i] = '\0';
	return (n1 + ft_strlen(src));
}
