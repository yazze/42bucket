/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/06 19:55:11 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/06 20:53:52 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strdup(const char *s)
{
	size_t	i;
	char	*s2;

	i = 0;
	s2 = (char *)malloc(ft_strlen(s) + 1);
	if (s2 != NULL)
		ft_strcpy(s2, s);
	else
		return (0);
	return (s2);
}
