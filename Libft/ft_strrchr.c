/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/06 21:26:52 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/27 22:31:37 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char		*ft_strrchr(const char *s, int c)
{
	int		i;
	char	*chr;

	i = 0;
	chr = NULL;
	while (*s != '\0')
	{
		if (*s == (char)c)
			chr = ((char *)s);
		s++;
	}
	if (*s == (char)c)
		chr = ((char *)s);
	return (chr);
}
