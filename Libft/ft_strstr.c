/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/06 22:38:20 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/31 18:23:04 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *s1, const char *s2)
{
	char	*string;

	if (ft_strlen(s2) == 0)
		return ((char *)s1);
	while (*s1)
	{
		if (ft_strncmp((char *)s1, s2, ft_strlen(s2)) == 0)
			return ((string = (char *)s1));
		s1++;
	}
	return (NULL);
}
