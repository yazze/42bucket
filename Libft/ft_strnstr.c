/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/06 22:39:09 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/31 19:31:58 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	int	i;
	size_t	len;

	i = s1[0];
	len = ft_strlen(s2);
	if (n < len)
                return (NULL);
	if (len == 0 || ft_strnequ(s1, s2, len))
		return ((char*)s1);
	if (*s1++ && --n)
		return (ft_strnstr(s1, s2, n));
	return (NULL);
}
