/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 19:20:52 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/31 18:15:57 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	n++;
	if (dest > src)
		while (--n > 0)
			((char*)dest)[n - 1] = ((const char*)src)[n - 1];
	if (--n && dest < src)
		ft_memcpy(dest, src, n);
	return (dest);
}
