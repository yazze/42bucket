/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 19:30:46 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/27 19:43:51 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char		*new;
	char		*ptr;
	unsigned int	i;

	i = 0;
	if (!s || !f)
		return (NULL);
	if (!(new = ft_memalloc(ft_strlen(s) + 1)))
		return (NULL);
	ptr = new;
	while (*s)
		*new++ = f(i++, *s++);
	*new++ = '\0';
	return (ptr);
}
