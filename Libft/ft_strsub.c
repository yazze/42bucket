/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 19:36:15 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/27 19:41:25 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*tr;
	int		nel;

	nel = len;
	if (len > 0)
		tr = ft_strnew(nel);
	if (nel == 0)
		tr = ft_strnew(0);
	if (!s || !tr)
		return (NULL);
	s = s + start;
	if (tr)
		ft_memcpy(tr, s, nel);
	tr[nel] = '\0';
	return (tr);
}
