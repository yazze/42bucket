/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 19:10:43 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/27 19:14:42 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int num;
	int j;

	j = 1;
	num = 0;
	while (*str == '\t' || *str == '\n' || *str == '\r'
			|| *str == '\v' || *str == '\f' || *str == ' ')
		str++;
	if (*str == '-')
	{
		j = -1;
		str++;
	}
	else if (*str == '+')
	{
		j = 1;
		str++;
	}
	if (!((*str >= '0' && *str <= '9')))
		return (0);
	while (*str >= '0' && *str <= '9')
		num = num * 10 + (*str++ - '0');
	return (num * j);
}
