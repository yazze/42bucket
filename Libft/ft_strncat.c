/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/06 21:22:55 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/27 19:35:44 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char		*ft_strncat(char *dest, const char *src, size_t n)
{
	size_t	i;
	char	*tmp;

	i = 0;
	tmp = dest;
	while (*tmp)
		tmp++;
	while (i++ < n && *src)
		*tmp++ = *src++;
	*tmp = '\0';
	return (dest);
}
