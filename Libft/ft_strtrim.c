/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yazzedin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 19:39:30 by yazzedin          #+#    #+#             */
/*   Updated: 2017/01/27 19:40:52 by yazzedin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strtrim(char const *s)
{
	char const	*ptr;
	size_t		len;

	if (s == NULL)
		return (NULL);
	while (*s == ' ' || *s == '\t' || *s == '\n')
		s++;
	if (*s == '\0')
		return (ft_strnew(0));
	ptr = s + ft_strlen(s) - 1;
	while (*ptr == ' ' || *ptr == '\t' || *ptr == '\n')
		ptr--;
	len = ptr - s + 1;
	return (ft_strsub(s, 0, len));
}
